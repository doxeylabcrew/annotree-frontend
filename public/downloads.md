AnnoTree is divided to three parts, frontend, backend and database.

* Database: https://bitbucket.org/doxeylabcrew/annotree-database
* Frontend: https://bitbucket.org/doxeylabcrew/annotree-frontend
* Backend: https://bitbucket.org/doxeylabcrew/annotree-backend

For users wishing to fully install AnnoTree, there is a complete log of our server set up in annotree-backend `README`.

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a><br />The data is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
