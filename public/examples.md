Example Queries
----

**Searching genomes including KEGG term nitrite reductase, and assimilatory nitrate reductase**

<img src="./kegg_search.gif"/>
<br/>

**Searching taxa clostridium genus and a specific clostridium genome**

<img src="./taxonomy_search.gif"/>
<br/>

**Searching genomes containing flagellin basal body domain and flagellin D3 domain**

<img src="./pfam_search.gif"/>
<br/>

**Uploading BLAST Result for Taxonomy Search**

1. Format XML output to have some E value cut off [filter out insignificant hits]
2. download XML2 in a single file
3. Upload that file to app under taxonomy section
4. Note that, only species id in BLAST file is considered
<br>
<img src="./blast_download_xml2_trimmed.gif"/>
<br>
<img src="./blast_xml2_button.jpg"/>

